import { useState } from "react";
import formContent from "../constants/form";

export default function Form() {
  const defaultFormState = {};
  const { initial, sending, sent, error } = formContent.buttonLabels;
  const [disabled, setDisabled] = useState(false);
  const [buttonLabel, setButtonLabel] = useState(initial);
  const [fields, setFields] = useState(defaultFormState);

  const simulateFetch = data =>
  //Simulate Slow Fetch
    new Promise(resolve =>
      setTimeout(() => {
        resolve(data);
      }, 2000)
    );

  const getData = async data => await simulateFetch(data);

  const handleChange = event => {
    setFields({ ...fields, [event.target.name]: event.target.value });
  };

  const handleSubmit = event => {
    setDisabled(true);
    setButtonLabel(sending);
    getData(fields)
      .then(response => {
        console.log(response);
        setDisabled(false);
        setButtonLabel(sent);
        setFields(defaultFormState);
        setTimeout(() => {
          setButtonLabel(initial);
        }, 2000);
      })
      .catch(() => {
        setDisabled(false);
        setButtonLabel(error);
      });

    event.preventDefault();
  };

  return (
    <form onSubmit={handleSubmit}>
      {formContent.questions.map(content => (
        <section key={content.title}>
          <h2>{content.title}</h2>
          {content.fields.map(field =>
            (() => {
              const { type, name, label, options } = field
              if (type === "text") {
                return <input
                  required
                  key={name}
                  type={type}
                  name={name}
                  placeholder={label}
                  value={fields[name]||""}
                  onChange={handleChange}
                />;
              } else if (type === "dropdown") {
                return (
                  <select
                    required
                    key={name}
                    type={type}
                    name={name}
                    value={fields[name]||""}
                    onChange={handleChange}
                  >
                    <option value="">Select {name}</option>
                    {options.map(option => (
                      <option key={option} value={option}>
                        {option}
                      </option>
                    ))}
                  </select>
                );
              }
            })()
          )}
        </section>
      ))}
      <input 
        type="submit"
        value={buttonLabel}
        disabled={disabled}
      />
    </form>
  );
}
